package usyd.it.olympics.gui;

import java.awt.BorderLayout;
import java.util.HashMap;

import javax.swing.JTextArea;
import usyd.it.olympics.OlympicsDBClient;

/**
 * Very simple home screen: Nothing fancy, just prints player details in a text area.
 * @author Bryn
 */
public class HomeScreen extends GuiScreen {

    private final JTextArea description;

    public HomeScreen(OlympicsDBClient r) {
        super(r);
        panel_.setLayout(new BorderLayout(0, 0));

        description = new JTextArea();
        description.setEditable(false);
        panel_.add(description);
    }


	public void showMemberDetails(HashMap<String, Object> details) {
		
        String message = "Hello" + " " + details.get("title") + " " + details.get("first_name") + " " + details.get("family_name");
        message = message.concat("\nYou are an: " +  OlympicsDBClient.getMemberType());
        message = message.concat("\nYou are from: " + details.get("country_name"));
        message = message.concat("\nYour live at: " + details.get("residence"));
        
        if(OlympicsDBClient.getMemberType().equals("athlete")){
        	int medaltally = (int) details.get("num_gold") + (int) details.get("num_silver") + (int) details.get("num_bronze");
	        message = message.concat("\nYour medal tally is: " + medaltally );
	        message = message.concat("\n\tGold: " +details.get("num_gold"));
	        message = message.concat("\n\tSilver: " + details.get("num_silver"));
	        message = message.concat("\n\tBronze: " +details.get("num_bronze"));
        }
        if(OlympicsDBClient.getMemberType().equals("staff")){
        	message = message.concat("\nYou have made " + details.get("num_bookings") + " bookings");
        }
        
        message = message.concat("\n");
        message = message.concat("\n");
        message = message.concat("\nTop Three Athletes: ");
        message = message.concat("\nRank 1: " + details.get("AthletePos1") + " number of medals = " + details.get("Medalcount1"));
        message = message.concat("\nRank 2: " + details.get("AthletePos2") + " number of medals = " + details.get("Medalcount2"));
        message = message.concat("\nRank 3: " + details.get("AthletePos3") + " number of medals = " + details.get("Medalcount3"));
        message = message.concat("\n");
        message = message.concat("\n");
        message = message.concat("\nTop Three Countries: ");
        message = message.concat("\nRank 1: " + details.get("CountryPos1") + " number of medals = " + details.get("Medalcountc1"));
        message = message.concat("\nRank 2: " + details.get("CountryPos2") + " number of medals = " + details.get("Medalcountc2"));
        message = message.concat("\nRank 3: " + details.get("CountryPos3") + " number of medals = " + details.get("Medalcountc3"));
        description.setText(message);	
        
	}
}
