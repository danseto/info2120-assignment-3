//jodu8426@uni
package usyd.it.olympics;


/**
 * Database back-end class for simple gui.
 * 
 * The DatabaseBackend class defined in this file holds all the methods to 
 * communicate with the database and pass the results back to the GUI.
 *
 *
 * Make sure you update the dbname variable to your own database name. You
 * can run this class on its own for testing without requiring the GUI.
 */
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;

/**
 * Database interfacing backend for client. This class uses JDBC to connect to
 * the database, and provides methods to obtain query data.
 * 
 * Most methods return database information in the form of HashMaps (sets of 
 * key-value pairs), or ArrayLists of HashMaps for multiple results.
 *
 * @author Bryn Jeffries {@literal <bryn.jeffries@sydney.edu.au>}
 */
public class DatabaseBackend {

    ///////////////////////////////
    /// DB Connection details
    ///////////////////////////////
    private final String dbUser;
    private final String dbPass;
	private final String connstring;


    ///////////////////////////////
    /// Student Defined Functions
    ///////////////////////////////

    /////  Login and Member  //////

    /**
     * Validate memberID details
     * 
     * Implements Core Functionality (a)
     *
     * @return true if username is for a valid memberID and password is correct
     * @throws OlympicsDBException 
     * @throws SQLException
     */
	public HashMap<String,Object> checkLogin(String member, char[] password) throws OlympicsDBException  {
        HashMap<String,Object> details = null;
        Connection conn = null;
        Statement stmt = null;
        try {
           conn = getConnection();
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			// Read committed because we want to see updates
	        // Don't forget you have memberID variables memberUser available to
	        // use in a query.
	        // Query whether login (memberID, password) is correct...
            
            boolean valid = false;
      	  	PreparedStatement membersql = conn.prepareStatement("SELECT * FROM member WHERE member_id = ?");
            membersql.setString(1, member);
            ResultSet members =  membersql.executeQuery();
            
            while(members.next())
            {
                //Retrieve by column name
                String member_id  = members.getString("member_id");
                String pass_word = members.getString("pass_word");
                String title = members.getString("title");
                String given_names = members.getString("given_names");
                String family_name = members.getString("family_name");
                int accommodationcode = members.getInt("accommodation");
                PreparedStatement findaccommodationname = conn.prepareStatement("SELECT place_name FROM Place WHERE place_id = ?");
                findaccommodationname.setInt(1, accommodationcode);
                ResultSet accommodationname =  findaccommodationname.executeQuery();
                String accommodation_name ="";
                while(accommodationname.next()){
                	accommodation_name = accommodationname.getString("place_name");
                }
                
                String country_code = members.getString("country_code");               
                PreparedStatement findcountryname = conn.prepareStatement("SELECT country_name FROM Country WHERE country_code = ?");
                findcountryname.setString(1, country_code);
                stmt = conn.createStatement();
                ResultSet countryname =  findcountryname.executeQuery();
                
                String country_name ="";
                while(countryname.next()){
                	country_name = countryname.getString("country_name");
                }
                valid = (member.equals(member_id) && new String(password).equals(pass_word));
                
                if (valid) 
                {
                	details = new HashMap<String,Object>();
	      	        // Populate with record data
	               	details.put("member_id", member_id);
	               	details.put("title", title);
	               	details.put("given_name", given_names);
	               	details.put("family_name", family_name);
	               	details.put("country_name", country_name);
	               	details.put("residence", accommodation_name);
	               	                	
	               	String member_type = "";
	               	
	               	PreparedStatement memberOfficial = conn.prepareStatement("SELECT member_id FROM Official WHERE member_id = ?");
	                memberOfficial.setString(1, member_id);
	                ResultSet memberOfficials = memberOfficial.executeQuery();
	                while(memberOfficials.next()){
	                   	member_type = "official";
	                   	OlympicsDBClient.setMemberType(member_type);
	                }
	                    
	                PreparedStatement memberStaff = conn.prepareStatement("SELECT member_id FROM Staff WHERE member_id = ?");
	                memberStaff.setString(1, member_id);
	                ResultSet memberStaffs = memberStaff.executeQuery();
	                while(memberStaffs.next()){
	                   	member_type = "staff";
	                   	OlympicsDBClient.setMemberType(member_type);
	                }
	                PreparedStatement memberAthlete = conn.prepareStatement("SELECT member_id FROM Athlete WHERE member_id = ?");
	                memberAthlete.setString(1, member_id);
	                ResultSet memberAthletes = memberAthlete.executeQuery();
	                while(memberAthletes.next()){
	                   	member_type = "athlete";
	                   	OlympicsDBClient.setMemberType(member_type);
	                }                  
	                    
	                details.put("member_type",member_type);
	                OlympicsDBClient.setMemberType(member_type);     
                }
                
            }
        } catch (Exception e) 
        {
      	    System.out.println(e.getMessage());
            throw new OlympicsDBException("Error checking login details", e);
        }finally{
        	reallyClose(conn);
        }
        return details;
    }

    /**
     * Obtain details for the current memberID
     * @param member 
     * @param member_type 
     *
     *
     * @return text to be displayed in the home screen
     * @throws OlympicsDBException
     */
	public HashMap<String, Object> getMemberDetails(String member) throws OlympicsDBException {

    	HashMap<String, Object> details = new HashMap<String, Object>();
    	Connection conn = null;
        try {
        	
            conn = getConnection();
            conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            //Chose transaction read commmitted because we want to read updated values when new rows are updated 
            PreparedStatement membersql = conn.prepareStatement("SELECT * FROM member WHERE member_id = ?");
            membersql.setString(1, member);
            ResultSet members =  membersql.executeQuery();
            while(members.next()){
                //Retrieve by column name
                String member_id  = members.getString("member_id");
                String title = members.getString("title");
                String given_names = members.getString("given_names");
                String family_name = members.getString("family_name");
                
                int accommodationcode = members.getInt("accommodation");
                PreparedStatement findaccommodationname = conn.prepareStatement("SELECT place_name FROM Place WHERE place_id = ?");
                findaccommodationname.setInt(1, accommodationcode);
                ResultSet accommodationname =  findaccommodationname.executeQuery();
                String accommodation_name ="";
                while(accommodationname.next())
                {
                	accommodation_name = accommodationname.getString("place_name");
                }
                
                String country_code = members.getString("country_code");
                PreparedStatement findcountryname = conn.prepareStatement("SELECT country_name FROM Country WHERE country_code = ?");
                findcountryname.setString(1, country_code);
                ResultSet countryname =  findcountryname.executeQuery();
                String country_name ="";
                while(countryname.next())
                {
                	country_name = countryname.getString("country_name");
                }
                
         	    // Populate with record data
                details.put("member_id", member_id);
                details.put("title", title);
                details.put("first_name", given_names);
                details.put("family_name", family_name);
                details.put("country_name", country_name);
                details.put("residence", accommodation_name);
                
            	String member_type = "";
               	
               	PreparedStatement memberOfficial = conn.prepareStatement("SELECT member_id FROM Official WHERE member_id = ?");
                memberOfficial.setString(1, member_id);
                ResultSet memberOfficials = memberOfficial.executeQuery();
                while(memberOfficials.next()){
                   	member_type = "official";
                }
                    
                PreparedStatement memberStaff = conn.prepareStatement("SELECT member_id FROM Staff WHERE member_id = ?");
                memberStaff.setString(1, member_id);
                ResultSet memberStaffs = memberStaff.executeQuery();
                while(memberStaffs.next()){
                   	member_type = "staff";
                }
                PreparedStatement memberAthlete = conn.prepareStatement("SELECT member_id FROM Athlete WHERE member_id = ?");
                memberAthlete.setString(1, member_id);
                ResultSet memberAthletes = memberAthlete.executeQuery();
                while(memberAthletes.next()){
                   	member_type = "athlete";
                }                  
                details.put("member_type",member_type);
               
               
                PreparedStatement countbookings = conn.prepareStatement("SELECT COUNT(booked_for) FROM Booking WHERE booked_for = ?");
                countbookings.setString(1, member);
                ResultSet bookings =  countbookings.executeQuery();
                int c = 0;
                while(bookings.next()){
                	c = bookings.getInt("count");
                }
                details.put("num_bookings", c);
                
                
                
                
                // Some attributes fetched may depend upon member_type
                // This is for an athlete
               
               if(member_type.equals("athlete")){
            	   int g1 = 0;
            	   int s1 = 0;
            	   int b1 = 0;
            	   int g2 = 0;
                   int s2 = 0;
                   int b2 = 0;
            	   PreparedStatement countmedals = conn.prepareStatement("SELECT medal FROM Participates WHERE athlete_id = ?");
                   countmedals.setString(1, member);
                   ResultSet medal =  countmedals.executeQuery();
                   while(medal.next())
                   {
                	   if (medal.getString("medal") == null) 
                	   {
                		   continue;
                	   }
                	   switch (medal.getString("medal"))
                	   {
                	   	case "G": 
                		   g1++;
                		   break;
                	   case "S": 
                		   s1++;
                		   break;
                	   case "B":
                		  	b1++;
                		  	break;
                	   }
                   }
                   
                   PreparedStatement countmedals2 = conn.prepareStatement("SELECT medal FROM Team JOIN TeamMember USING (team_name) WHERE athlete_id = ?");
                   countmedals2.setString(1, member);
                   ResultSet medal2 =  countmedals2.executeQuery();
                   while(medal2.next())
                   {
                	   if (medal2.getString("medal") == null) 
                	   {
                		   continue;
                	   }
                	   switch (medal2.getString("medal"))
                	   {
                	   	case "G": 
                		   g2++;
                		   break;
                	   case "S": 
                		   s2++;
                		   break;
                	   case "B":
                		  	b2++;
                		  	break;
                	   }
                   }
                   details.put("num_gold", g1+g2);
	               details.put("num_silver",s1+s2);
	               details.put("num_bronze", b1+b2);
               }
            	   /**int g1 = 0;
                   int s1 = 0;
                   int b1 = 0;
                   int g2 = 0;
                   int s2 = 0;
                   int b2 = 0;
            	   PreparedStatement countmedalsgold = conn.prepareStatement("SELECT COUNT(*) AS count FROM Participates WHERE athlete_id = ? AND medal = 'G'");
                   countmedalsgold.setString(1, member);
                   ResultSet gold =  countmedalsgold.executeQuery();
                   while(gold.next())
                   {
                	   g1 = gold.getInt("count");
                   }      
                   PreparedStatement countmedalssilver = conn.prepareStatement("SELECT COUNT(*) AS count FROM Participates WHERE athlete_id = ? AND medal = 'S'");
                   countmedalssilver.setString(1, member);
                   ResultSet silver =  countmedalssilver.executeQuery();
                   while(silver.next())
                   {
                	   s1 = silver.getInt("count");
                   }  
                   PreparedStatement countmedalsbronze = conn.prepareStatement("SELECT COUNT(*) AS count FROM Participates WHERE athlete_id = ? AND medal = 'B'");
                   countmedalsbronze.setString(1, member);
                   ResultSet bronze =  countmedalsbronze.executeQuery();
                   while(bronze.next())
                   {
                	   b1 = bronze.getInt("count");
                   }   
                   
                   PreparedStatement countteamgold = conn.prepareStatement("SELECT COUNT(medal) AS count FROM Team JOIN TeamMember USING (team_name) "
                   		+ "WHERE athlete_id = ? AND medal = 'G'");
                   countteamgold.setString(1, member);
                   ResultSet teamg =  countteamgold.executeQuery();
                   while(teamg.next())
                   {
                	   g2 = teamg.getInt("count");
                   }
                   
                   PreparedStatement countteamsilver = conn.prepareStatement("SELECT COUNT(medal) AS count FROM Team JOIN TeamMember USING (team_name) "
                      		+ "WHERE athlete_id = ? AND medal = 'S'");
                   countteamsilver.setString(1, member);
                   ResultSet teamsil =  countteamsilver.executeQuery();
                   while(teamsil.next())
                   {
                   	   s2 = teamsil.getInt("count");
                   }
                   PreparedStatement countteambronze = conn.prepareStatement("SELECT COUNT(medal) AS count FROM Team JOIN TeamMember USING (team_name) "
                         		+ "WHERE athlete_id = ? AND medal = 'B'");
                   countteambronze.setString(1, member);
                   ResultSet teamb =  countteambronze.executeQuery();
                   while(teamb.next())
                   {
                       b2 = teamb.getInt("count");
                   }
                   
	               details.put("num_gold", g1+g2);
	               details.put("num_silver",s1+s2);
	               details.put("num_bronze", b1+b2);
                   }
                **/
	              PreparedStatement topAthletes = conn.prepareStatement("Select count(medal), athlete_id, family_name, given_names from participates "
	  	               	+ "inner join member on (participates.athlete_id = member.member_id) group by (athlete_id, family_name, given_names) order by count desc Limit 3");
	                 	ResultSet athletes =  topAthletes.executeQuery();
	              for(Integer i = 1; i < 4; i++){
	               	   String a = "AthletePos";
	               	   athletes.next();
	               	   a += i.toString();
	               	   String athletename = "";
	               	   athletename = athletes.getString("given_names") + " " + athletes.getString("family_name");
	               	   details.put(a, athletename);
	              	   String amedals = "";
	               	   amedals = "Medalcount" + i.toString();
	               	   details.put(amedals, athletes.getInt("count"));
	              }
	              PreparedStatement topCountries= conn.prepareStatement("Select count(medal), country_name from participates inner join member on (participates.athlete_id = "
	 	           		+ "member.member_id) join Country USING(country_code) group by (athlete_id, country_name) order by count desc Limit 3");
	 	          ResultSet countries =  topCountries.executeQuery();
	 	          for(Integer i = 1; i < 4; i++){
	 		       	  String a = "CountryPos";
	 		       	  countries.next();
	 		      	  a += i.toString();
	 		       	  String country = "";
	 		       	  country = countries.getString("country_name");
	 		      	  details.put(a, country);
	 		       	  String amedal = "";
	 		       	  amedal = "Medalcountc" + i.toString();
	 		       	  details.put(amedal, countries.getInt("count"));
	 		      }
                
           	}
        } 
        catch (Exception e){
      	    System.out.println(e.getMessage());
            throw new OlympicsDBException("Error inputting member details", e);
        }finally{
        	reallyClose(conn);
        }
        return details;
    }


    //////////  Events  //////////

    /**
     * Get all of the events listed in the olympics for a given sport
     *
     * @param sportname the ID of the sport we are filtering by
     * @return List of the events for that sport
     * @throws OlympicsDBException
     */
    ArrayList<HashMap<String, Object>> getEventsOfSport(Integer sportname) throws OlympicsDBException {

        ArrayList<HashMap<String, Object>> events = new ArrayList<>();
        Connection conn = null;
        try {
		    conn = getConnection();
		    conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            //Chose transaction read commmitted because we want to read updated values when new rows are updated 
		    PreparedStatement getEvents;
			getEvents = conn.prepareStatement("SELECT * FROM Sport JOIN Event USING (sport_id) WHERE sport_id = ?");
			getEvents.setInt(1,sportname);
	        ResultSet event = getEvents.executeQuery();
	        while(event.next()){
		        HashMap<String,Object> event1 = new HashMap<String,Object>();
		        event1.put("event_id", event.getInt("event_id"));
		        event1.put("sport_id", event.getInt("sport_id"));
		        event1.put("event_name", event.getString("event_name"));
		        event1.put("event_gender", event.getString("event_gender"));
		        int sportvenue = event.getInt("sport_venue");
                PreparedStatement findvenuename = conn.prepareStatement("SELECT place_name FROM Place WHERE place_id = ?");
                findvenuename.setInt(1, sportvenue);
                ResultSet venuename =  findvenuename.executeQuery();
                while(venuename.next())
                {
                	event1.put("sport_venue", venuename.getString("place_name"));
                }
		        event1.put("event_start", event.getTimestamp("event_start"));
		        events.add(event1);		      
	        }
		} catch (SQLException e){
			e.printStackTrace();
		}finally{
        	reallyClose(conn);
        }

        return events;
    }

    /**
     * Retrieve the results for a single event
     * @param eventId the key of the event
     * @return a hashmap for each result in the event.
     * @throws OlympicsDBException
     */
    ArrayList<HashMap<String, Object>> getResultsOfEvent(Integer eventId) throws OlympicsDBException {
    	ArrayList<HashMap<String, Object>> results = new ArrayList<>();
    	Connection conn = null;
    	try 
    	{
		    conn = getConnection(); 
		    conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            //Chose transaction read commmitted because we want to read updated values when new rows are updated 
		    //Individual event
		    PreparedStatement getresults = conn.prepareStatement("SELECT event_id, athlete_id, Participates.medal AS individualmedal, team_name, Team.medal AS teammedal "
		    		+ "FROM Event LEFT OUTER JOIN Participates USING (event_id) LEFT OUTER JOIN Team USING (event_id) WHERE event_id = ?;");
		    getresults.setInt(1, eventId);
		    ResultSet result = getresults.executeQuery();
		    while(result.next())
		    {
		    	HashMap<String,Object> result1 = new HashMap<String,Object>();
		    	String athlete_id = result.getString("athlete_id");
		    	if (athlete_id != null)
		    	{
		    		String medal = result.getString("individualmedal");
		    	   if (medal == null) 
             	   {
		    		   result1.put("medal", "");
             		  
             	   }
		    	   else
		    	   {
             	   switch (medal)
             	   {
             	   	case "G": 
             	   		result1.put("medal", "Gold");
             		   break;
             	   case "S": 
             		  result1.put("medal", "Silver");
             		   break;
             	   case "B":
             		  result1.put("medal", "Bronze");
             		  	break;
             	   }
		    	   }
		    		/**
		    		if( medal == "G"){
			        	result1.put("medal", "Gold");
			        }else if( medal == "S"){
			        	result1.put("medal", "Silver");
			        }else if( medal == "B"){
			        	result1.put("medal", "Bronze");
			        }else {
			        	result1.put("medal", "");
			        }
			        **/
		    		PreparedStatement individual = conn.prepareStatement("SELECT * FROM Member JOIN Country USING (country_code) WHERE member_id = ?");
		    		individual.setString(1, athlete_id);
		    		ResultSet individualresults = individual.executeQuery();
		    		while(individualresults.next())
		    		{
				        String fullname = individualresults.getString("family_name").toUpperCase() + ", " + individualresults.getString("given_names");
	        			result1.put("participant", fullname);
	        			String country_name = individualresults.getString("country_name");
	        			result1.put("country_name", country_name);
	        			results.add(result1);
		    		}
		    	}
		    	else
		    	{
		    		String team_name = result.getString("team_name");
		    		result1.put("participant", team_name);	
		    		String medal = result.getString("teammedal");
			    	   if (medal == null) 
	             	   {
			    		   result1.put("medal", "");
	             	   }
			    	   else
			    	   {
	             	   switch (medal)
	             	   {
	             	   	case "G": 
	             	   		result1.put("medal", "Gold");
	             		   break;
	             	   case "S": 
	             		  result1.put("medal", "Silver");
	             		   break;
	             	   case "B":
	             		  result1.put("medal", "Bronze");
	             		  	break;
	             	   }
	             	   }
		    		/**
		    		if( medal == "G"){
			        	result1.put("medal", "Gold");
			        }else if( medal == "S"){
			        	result1.put("medal", "Silver");
			        }else if( medal == "B"){
			        	result1.put("medal", "Bronze");
			        }else {
			        	result1.put("medal", "");
			        **/
		    		PreparedStatement team = conn.prepareStatement("SELECT * FROM Team JOIN Country using (country_code) WHERE team_name = ? AND event_id = ?");
		    		team.setString(1, team_name);
		    		team.setInt(2, eventId);
		    		ResultSet teamresults = team.executeQuery();
		    		while(teamresults.next())
		    		{
		    			result1.put("country_name", teamresults.getString("country_name"));    			
				        results.add(result1);
		    		}
		    	}
		    }
		    /**
		    PreparedStatement getresults;
			getresults = conn.prepareStatement("SELECT * FROM Participates JOIN Member on "
					+ "Participates.athlete_id = Member.member_id JOIN Country using (country_code) WHERE event_id = ? ");
			getresults.setInt(1, eventId);
	        ResultSet result = getresults.executeQuery();
	        if(result.next() == true){
		        while(result.next()){
			        HashMap<String,Object> result1 = new HashMap<String,Object>();
			        result1.put("country_name", result.getString("country_name"));
			        String medal = result.getString("medal");
			        if( medal == null){
			        	result1.put("medal", "");
			        }else if( medal == "G"){
			        	result1.put("medal", "Gold");
			        }else if( medal == "S"){
			        	result1.put("medal", "Silver");
			        }else if(medal == "B"){
			        	result1.put("medal", "Bronze");
			        }
			        PreparedStatement bookedby = conn.prepareStatement("SELECT family_name, given_names FROM Member WHERE member_id = ?");
	        		bookedby.setString(1, result.getString("athlete_id"));
	        		ResultSet bookedby1 = bookedby.executeQuery();
	        		String fullname = "";
	        		while(bookedby1.next()){
	        			fullname = bookedby1.getString("family_name").toUpperCase() + ", " + bookedby1.getString("given_names");
	        			result1.put("participant", fullname);
	        		}
		        results.add(result1);
		        }
	        }else{
	        	PreparedStatement getresultsc;
	        	getresultsc = conn.prepareStatement("SELECT * FROM Team JOIN Country using (country_code) WHERE event_id = ? ");
				getresultsc.setInt(1, eventId);
		        ResultSet resultc = getresultsc.executeQuery();
		        while(resultc.next()){
		        	HashMap<String,Object> result1 = new HashMap<String,Object>();
				    result1.put("country_name", resultc.getString("country_name"));
				    String medal = resultc.getString("medal");
			        if( medal == null){
			        	result1.put("medal", "");
			        }else if( medal =="G"){
				      	result1.put("medal", "Gold");
				    }else if(medal == "S"){
				       	result1.put("medal", "Silver");
				    }else if(medal == "B"){
				       	result1.put("medal", "Bronze");
				    }
				    result1.put("participant", resultc.getString("team_name"));
		        	results.add(result1);
		        }
	        }
	        **/
    	}
		catch (SQLException e){
			e.printStackTrace();
		}finally{
        	reallyClose(conn);
        }
        return results;
    }


///////   Journeys    ////////

  /**
   * Array list of journeys from one place to another on a given date
   * @param journeyDate the date of the journey
   * @param fromPlace the origin, starting place.
   * @param toPlace the destination, place to go to.
   * @return a list of all journeys from the origin to destination
   */
  ArrayList<HashMap<String, Object>> findJourneys(String fromPlace, String toPlace, Date journeyDate) throws OlympicsDBException {
      ArrayList<HashMap<String, Object>> journeys = new ArrayList<>();
      Connection conn = null;
      
      try 
      {
        conn = getConnection();
        conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        //Chose transaction read commmitted because we want to read updated values 
    	  SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");  
    	  String journeyDate2 = ft.format(journeyDate);
          PreparedStatement journeysql = conn.prepareStatement(
        		  "SELECT journey_id, depart_time, vehicle_code, nbooked, arrive_time, p1.place_id AS from_place, p1.place_name AS from_place_name, "
        		  + "p1.address AS from_address, p2.place_id AS to_place, p2.place_name AS to_place_name, p2.address AS to_address, capacity FROM Journey "
        		  + "LEFT OUTER JOIN PLACE p1 ON (from_place = p1.place_id) LEFT OUTER JOIN PLACE p2 ON (to_place = p2.place_id) LEFT OUTER JOIN Vehicle USING (vehicle_code) WHERE p1.place_name = "
        		  + "? AND p2.place_name = ? AND to_char(depart_time, 'yyyy-MM-dd')=  ?");
    	  
    	 // PreparedStatement journeysql = conn.prepareStatement("SELECT * FROM listjourneys(?,?,?)");
    	  
      	  journeysql.setString(1, fromPlace);
      	  journeysql.setString(2, toPlace);
      	  journeysql.setString(3, journeyDate2);
          ResultSet journeylist = journeysql.executeQuery();
          System.out.println(journeysql);
          while (journeylist.next())
          {
        	HashMap<String,Object> journey1 = new HashMap<String,Object>();
          	int journey_id = journeylist.getInt("journey_id");
          	Timestamp depart_time = journeylist.getTimestamp("depart_time");
          	Timestamp arrive_time = journeylist.getTimestamp("arrive_time");
      		String from_place_name = journeylist.getString("from_place_name");
      		String to_place_name = journeylist.getString("to_place_name");
      		String vehicle_code = journeylist.getString("vehicle_code");
      		int nbooked = journeylist.getInt("nbooked");
      		int capacity = journeylist.getInt("capacity");
      		
      		journey1.put("journey_id", journey_id);
      		journey1.put("vehicle_code", vehicle_code);
      		journey1.put("origin_name", from_place_name);
      		journey1.put("dest_name", to_place_name);
      		journey1.put("when_departs", depart_time);
      		journey1.put("when_arrives", arrive_time);
      		journey1.put("number of bookings", nbooked);
      		journey1.put("available_seats", (capacity - nbooked));
      		journeys.add(journey1);
          }
      }catch (Exception e)
      {
    	  System.out.println(e.getMessage());
          throw new OlympicsDBException("Error inputting journey details", e);
      }finally{
      	reallyClose(conn);
      }
  
      return journeys;   
      
  }
  
  ArrayList<HashMap<String,Object>> getMemberBookings(String memberID) throws OlympicsDBException {
      ArrayList<HashMap<String,Object>> bookings = new ArrayList<HashMap<String,Object>>();
      Connection conn = null;
      
      try 
      {   
    	  conn = getConnection();
    	  conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
          //Chose transaction read commmitted because we want to read updated values when new rows are updated 
    	  PreparedStatement bookingssql = conn.prepareStatement(
          "SELECT journey_id, depart_time, arrive_time, from_place, to_place, vehicle_code"
          + " FROM Booking JOIN Journey USING (journey_id) WHERE booked_for = ?");
          bookingssql.setString(1, memberID);
          ResultSet bookingslist = bookingssql.executeQuery();
          while (bookingslist.next())
          {
        	HashMap<String,Object> booking = new HashMap<String,Object>();
          	Timestamp depart_time = bookingslist.getTimestamp("depart_time");
          	Timestamp arrive_time = bookingslist.getTimestamp("arrive_time");
          	 
          	int fromdest = bookingslist.getInt("from_place");
            PreparedStatement findfromdest = conn.prepareStatement("SELECT place_name FROM Place WHERE place_id = ?");
            findfromdest.setInt(1, fromdest);
            ResultSet resultfromdest =  findfromdest.executeQuery();
            String from ="";
            while(resultfromdest.next())
            {
            	from = resultfromdest.getString("place_name");
            	booking.put("origin_name", from);
            }
            int todest = bookingslist.getInt("to_place");
            PreparedStatement findtodest = conn.prepareStatement("SELECT place_name FROM Place WHERE place_id = ?");
            findtodest.setInt(1, todest);
            ResultSet resultdest =  findtodest.executeQuery();
            String to ="";
            while(resultdest.next())
            {
            	to = resultdest.getString("place_name");
            	booking.put("dest_name",to);
            }
            
      		String vehicle_code = bookingslist.getString("vehicle_code");
      		int journey_id = bookingslist.getInt("journey_id");
      		booking.put("journey_id", journey_id);
      		booking.put("vehicle_code", vehicle_code);
      		booking.put("when_departs", depart_time);
      		booking.put("when_arrives", arrive_time);
      		bookings.add(booking);
          }
      }
      catch (Exception e)
      {
    	  System.out.println(e.getMessage());
          throw new OlympicsDBException("Error retrieving journey details for member", e);
      }finally{
      	reallyClose(conn);
      }
      return bookings;
  }
                
    /**
     * Get details for a specific journey
     * 
     * @return Various details of journey - see JourneyDetails.java
     * @throws OlympicsDBException
     * @param journey_id
     */
  public HashMap<String,Object> getJourneyDetails(int journey_id) throws OlympicsDBException {
      // See the constructor in BayDetails.java
  	HashMap<String,Object> details = new HashMap<String,Object>();
  	Connection conn = null;
      try
          {
           conn = getConnection();
   			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
           PreparedStatement journeysql = conn.prepareStatement(
               "SELECT * FROM Journey JOIN Vehicle USING (vehicle_code) WHERE journey_id = ?");
           journeysql.setInt(1, journey_id);
           ResultSet journey = journeysql.executeQuery();
           while (journey.next())
           	{            	
               Timestamp depart_time = journey.getTimestamp("depart_time");
               Timestamp arrive_time = journey.getTimestamp("arrive_time");
               int from_place = journey.getInt("from_place");
	             int to_place = journey.getInt("to_place");
	             String vehicle_code = journey.getString("vehicle_code");
	             int nbooked = journey.getInt("nbooked");
	             int capacity = journey.getInt("capacity");
	             
	             PreparedStatement namefrom = conn.prepareStatement("SELECT * FROM Place WHERE place_id = ?");
	             namefrom.setInt(1, from_place);
	             ResultSet fromname = namefrom.executeQuery();
	             while(fromname.next()){
	            	 String fromPlace = fromname.getString("place_name");
	            	 details.put("origin_name", fromPlace);
	             }
	             PreparedStatement toname = conn.prepareStatement("SELECT * FROM Place WHERE place_id = ?");
	             toname.setInt(1, to_place);
	             ResultSet nameto = toname.executeQuery();
	             while(nameto.next()){
	            	 String toPlace = nameto.getString("place_name");  
	            	 details.put("dest_name", toPlace);
	             }
	            
	             details.put("journey_id", journey_id);
	             details.put("when_departs", depart_time);
	             details.put("when_arrives", arrive_time);
	             details.put("vehicle_code", vehicle_code);
	             details.put("nbooked", nbooked);
	             details.put("capacity", capacity);
           	}
           }
           catch (Exception e)
           {
        	   System.out.println(e.getMessage());
               throw new OlympicsDBException("Error inputting journey details for journey_id", e);
           }
      	   finally{
           	reallyClose(conn);
           }
          return details;
      }

    
    public HashMap<String,Object> makeBooking(String byStaff, String forMember, String vehiclecode, Date departs) throws OlympicsDBException 
    {
    	HashMap<String,Object> booking = null;
    	
    	
    	Connection conn = null;
    	try 
    	{
    		conn = getConnection();
    		String member_type = "";
    		PreparedStatement memberStaff = conn.prepareStatement("SELECT member_id FROM Staff WHERE member_id = ?");
	        memberStaff.setString(1, byStaff);
	        ResultSet memberStaffs = memberStaff.executeQuery();
	        while(memberStaffs.next()){
	           	member_type = "staff";
	        }
	        if(member_type.equals("staff")){
	    		conn.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
	    		conn.setAutoCommit(false); // next SQL statement starts transaction
	    		Timestamp departure = new Timestamp(departs.getTime());
	    		PreparedStatement stmt = conn.prepareStatement("SELECT journey_id, capacity, nbooked, vehicle_code FROM Vehicle JOIN Journey USING(vehicle_code) "
	    				+ "WHERE VEHICLE_CODE = ? AND depart_time = date_trunc('second', ?::TIMESTAMP)");
	    		/*PreparedStatement stmt = conn.prepareStatement("SELECT * FROM capacities(?,?)");*/
	    		stmt.setString(1, vehiclecode);
	    		stmt.setTimestamp(2, departure);
	    		ResultSet rset = stmt.executeQuery();
	    		while (rset.next())
	    		{ 
    			int capacity = rset.getInt("capacity");
    			int nbooked = rset.getInt("nbooked");
    			int journey_id = rset.getInt("journey_id");
    			String vehicle_code = rset.getString("vehicle_code");
    			if (capacity > nbooked)
    			{
    				PreparedStatement stmt2 =  conn.prepareStatement("UPDATE Journey SET nbooked = nbooked+1 WHERE vehicle_code = ? AND depart_time = ?");
    				stmt2.setString(1, vehicle_code);
    				stmt2.setTimestamp(2, departure);
    				stmt2.executeUpdate();
    				PreparedStatement stmt3 = conn.prepareStatement("INSERT INTO Booking VALUES (?,?,?,?)");
    				stmt3.setString(1, forMember);
    				stmt3.setString(2, byStaff);
    				stmt3.setTimestamp(3, departure);
    				stmt3.setInt(4, journey_id);
    				stmt3.executeUpdate();
    				conn.commit();
    				
    				conn.setAutoCommit(true);
    	    		Timestamp ts = new Timestamp(System.currentTimeMillis());
    				booking = new HashMap<String,Object>();
    				booking.put("when_booked", ts);
    		   		booking.put("journey_id", journey_id);
    		    		
    		   		PreparedStatement bookedby = conn.prepareStatement("SELECT family_name, given_names FROM Member WHERE member_id = ?");
    	        		bookedby.setString(1, byStaff);
    	        		ResultSet bookedby1 = bookedby.executeQuery();
    	        		String fullname = "";
    	        		while(bookedby1.next()){
    	        			fullname = bookedby1.getString("family_name").toUpperCase() + ", " + bookedby1.getString("given_names");
    	        		}
    	        		booking.put("bookedby_name",fullname);
    	        		PreparedStatement bookedfor = conn.prepareStatement("SELECT * FROM Member WHERE member_id = ?");
    	        		bookedfor.setString(1, forMember);
    	        		ResultSet bookedfor1 = bookedfor.executeQuery();
    	        		String fullnamefor = "";
    	        		while(bookedfor1.next()){
    	        			fullnamefor = bookedfor1.getString("family_name").toUpperCase() + ", " + bookedfor1.getString("given_names");
    	        		}    	        		
    	        		booking.put("bookedfor_name",fullnamefor);
    	    	        booking.put("vehicle", vehicle_code);
    	    	    	booking.put("when_departs", departs);
    	    	    	PreparedStatement details = conn.prepareStatement("SELECT journey_id, vehicle_code, depart_time, from_place,"
    	        				+ " to_place, booked_by, booked_for, when_booked, arrive_time FROM Booking JOIN Journey USING(journey_id)"
    	        				+ " WHERE  booked_for =? AND journey_id = ?");
    	        		details.setString(1, forMember);
     	        		details.setInt(2, journey_id);
    	        		ResultSet rsets = details.executeQuery();
    	        		while(rsets.next())
    	        		{	
	    	                Timestamp arrive_time = rsets.getTimestamp("arrive_time");
	    	    			Timestamp when_booked = rsets.getTimestamp("when_booked");
	    	    			
	    	    			int fromdest = rsets.getInt("from_place");
	    	                PreparedStatement findfromdest = conn.prepareStatement("SELECT place_name FROM Place WHERE place_id = ?");
	    	                findfromdest.setInt(1, fromdest);
	    	                ResultSet resultfromdest =  findfromdest.executeQuery();
	    	                String from ="";
	    	                while(resultfromdest.next())
	    	                {
	    	                	from = resultfromdest.getString("place_name");
	    	                	booking.put("origin_name", from);
	    	                }
	    	                int todest = rsets.getInt("to_place");
	    	                PreparedStatement findtodest = conn.prepareStatement("SELECT place_name FROM Place WHERE place_id = ?");
	    	                findtodest.setInt(1, todest);
	    	                ResultSet resultdest =  findtodest.executeQuery();
	    	                String to ="";
	    	                while(resultdest.next())
	    	                {
	    	                	to = resultdest.getString("place_name");
	    	                	booking.put("dest_name",to);
	    	                }
	    	    	    	booking.put("when_booked", when_booked);
	    	    	    	booking.put("when_arrives", arrive_time);
    	        		} 		      		
    				
    				}
    				else
    				{
    					conn.rollback();
    				}
	    		}
    			stmt.close();
	        }
    	}
    	     catch (SQLException e) 
             { 
        	   System.out.println(e.getMessage());
               throw new OlympicsDBException("Unable to make booking for member", e);
             }finally{ 	
             	reallyClose(conn);
             }
    	
    	return booking;
    }
    
    public HashMap<String,Object> getBookingDetails(String memberID, Integer journeyId) throws OlympicsDBException {
    	HashMap<String,Object> booking = null;
    	booking = new HashMap<String,Object>();
    	Connection conn = null;
    	try{
    		conn = getConnection();
    		conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
              //Chose transaction read commmitted because we want to read updated values when new rows are updated 
    		PreparedStatement stmt = conn.prepareStatement("SELECT journey_id, vehicle_code, depart_time, from_place,"
    				+ " to_place, booked_by, booked_for, when_booked, arrive_time FROM Booking JOIN Journey USING(journey_id)"
    				+ " WHERE (booked_by =? OR booked_for =?) AND journey_id = ? ORDER BY journey_id,depart_time,from_place");
    		/*PreparedStatement stmt = conn.prepareStatement("SELECT * FROM listbookingdetails(?,?)");*/
    		stmt.setString(1, memberID);
    		stmt.setString(2, memberID);
    		stmt.setInt(3, journeyId);
    		ResultSet rset = stmt.executeQuery();
    		while (rset.next()) {
    			Timestamp depart_time = rset.getTimestamp("depart_time");
    			Timestamp arrive_time = rset.getTimestamp("arrive_time");
                String vehicle_code = rset.getString("vehicle_code");
    			String booked_by = rset.getString("booked_by");
    			String booked_for = rset.getString("booked_for");
    			Timestamp when_booked = rset.getTimestamp("when_booked");
    			
    			PreparedStatement bookedby = conn.prepareStatement("SELECT family_name, given_names FROM Member WHERE member_id = ?");
        		bookedby.setString(1, booked_by);
        		ResultSet bookedby1 = bookedby.executeQuery();
        		String fullname = "";
        		while(bookedby1.next()){
        			fullname = bookedby1.getString("family_name").toUpperCase() + ", " + bookedby1.getString("given_names");
        		}
        		booking.put("bookedby_name",fullname);
        		PreparedStatement bookedfor = conn.prepareStatement("SELECT * FROM Member WHERE member_id = ?");
        		bookedfor.setString(1, memberID);
        		ResultSet bookedfor1 = bookedfor.executeQuery();
        		String fullnamefor = "";
        		while(bookedfor1.next()){
        			fullnamefor = bookedfor1.getString("family_name").toUpperCase() + ", " + bookedfor1.getString("given_names");
        		}
        		booking.put("bookedfor_name",fullnamefor);
    			
            	booking.put("journey_id", journeyId);
    	        booking.put("vehicle", vehicle_code);
    	    	booking.put("when_departs", depart_time);
    	    	int fromdest = rset.getInt("from_place");
                PreparedStatement findfromdest = conn.prepareStatement("SELECT place_name FROM Place WHERE place_id = ?");
                findfromdest.setInt(1, fromdest);
                ResultSet resultfromdest =  findfromdest.executeQuery();
                String from ="";
                while(resultfromdest.next())
                {
                	from = resultfromdest.getString("place_name");
                	booking.put("origin_name", from);
                }
                int todest = rset.getInt("to_place");
                PreparedStatement findtodest = conn.prepareStatement("SELECT place_name FROM Place WHERE place_id = ?");
                findtodest.setInt(1, todest);
                ResultSet resultdest =  findtodest.executeQuery();
                String to ="";
                while(resultdest.next())
                {
                	to = resultdest.getString("place_name");
                	booking.put("dest_name",to);
                }
    	    	booking.put("bookedby", booked_by);
    	    	booking.put("bookedfor", booked_for);
    	    	booking.put("when_booked", when_booked);
    	    	booking.put("when_arrives", arrive_time);
    	    	    		
    		}
      	}
    	catch (SQLException e)
    	{
    		System.out.println(e.getMessage());
            throw new OlympicsDBException("Cannot retrieve booking details", e);
    	}finally{
        	reallyClose(conn);
        }
        return booking;
    }
    
	public ArrayList<HashMap<String, Object>> getSports() throws OlympicsDBException {
		ArrayList<HashMap<String,Object>> sports = new ArrayList<HashMap<String,Object>>();
		Connection conn = null;
		try {
		    conn = getConnection(); 
		    conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            //Chose transaction read commmitted because we want to read updated values when new rows are updated 
		    PreparedStatement getsport;
			getsport = conn.prepareStatement("SELECT * FROM Sport ");
			
	        ResultSet sport = getsport.executeQuery();
	        while(sport.next()){
		        HashMap<String,Object> sport1 = new HashMap<String,Object>();
		        sport1.put("sport_id", sport.getInt("sport_id"));
		        sport1.put("sport_name", sport.getString("sport_name"));
		        sport1.put("discipline", sport.getString("discipline"));
		        sports.add(sport1);
	        }
		} catch (SQLException e){
			e.printStackTrace();
		}finally{
        	reallyClose(conn);
        }
		return sports;
	}
	
	public ArrayList<HashMap<String, Object>> getLocations() throws OlympicsDBException {
		ArrayList<HashMap<String,Object>> locations = new ArrayList<HashMap<String,Object>>();
		Connection conn = null;
		try {
		    conn = getConnection(); 
		    conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            //Chose transaction read commmitted because we want to read updated values when new rows are updated 
		    PreparedStatement getlocation;
			getlocation = conn.prepareStatement("SELECT * FROM Place ");
			
	        ResultSet location = getlocation.executeQuery();
	        while(location.next()){
		        HashMap<String,Object> location1 = new HashMap<String,Object>();
		        location1.put("place_name", location.getString("place_name"));
		        locations.add(location1);
	        }
		} catch (SQLException e){
			e.printStackTrace();
		}finally{
        	reallyClose(conn);
        }
		return locations;
	}


    /////////////////////////////////////////
    /// Functions below don't need
    /// to be touched.
    ///
    /// They are for connecting and handling errors!!
    /////////////////////////////////////////

    /**
     * Default constructor that simply loads the JDBC driver and sets to the
     * connection details.
     *
     * @throws ClassNotFoundException if the specified JDBC driver can't be
     * found.
     * @throws OlympicsDBException anything else
     */
    DatabaseBackend(InputStream config) throws ClassNotFoundException, OlympicsDBException {
    	Properties props = new Properties();
    	try {
			props.load(config);
		} catch (IOException e) {
			throw new OlympicsDBException("Couldn't read config data",e);
		}

    	dbUser = props.getProperty("username");
    	dbPass = props.getProperty("userpass");
    	String port = props.getProperty("port");
    	String dbname = props.getProperty("dbname");
    	String server = props.getProperty("address");;
    	
        // Load JDBC driver and setup connection details
    	String vendor = props.getProperty("dbvendor");
		if(vendor==null) {
    		throw new OlympicsDBException("No vendor config data");
    	} else if ("postgresql".equals(vendor)) { 
    		Class.forName("org.postgresql.Driver");
    		connstring = "jdbc:postgresql://" + server + ":" + port + "/" + dbname;
    	} else if ("oracle".equals(vendor)) {
    		Class.forName("oracle.jdbc.driver.OracleDriver");
    		connstring = "jdbc:oracle:thin:@" + server + ":" + port + ":" + dbname;
    	} else throw new OlympicsDBException("Unknown database vendor: " + vendor);
		
		// test the connection
		Connection conn = null;
		try {
			conn = getConnection();
		} catch (SQLException e) {
			throw new OlympicsDBException("Couldn't open connection", e);
		} finally {
			reallyClose(conn);
		}
    }

	/**
	 * Utility method to ensure a connection is closed without 
	 * generating any exceptions
	 * @param conn Database connection
	 */
	private void reallyClose(Connection conn) {
		if(conn!=null)
			try {
				conn.close();
			} catch (SQLException ignored) {}
	}

    /**
     * Construct object with open connection using configured login details
     * @return database connection
     * @throws SQLException if a DB connection cannot be established
     */
    private Connection getConnection() throws SQLException {
        Connection conn;
        conn = DriverManager.getConnection(connstring, dbUser, dbPass);
        return conn;
    }    
}